import React, { Component } from "react";
import classnames from "classnames";
import { Map, Marker, TileLayer } from "react-leaflet";
import Vehicle from "./Vehicle";
import { divIcon } from "leaflet";

const position = [50.005039, 36.230365];
const cover = { position: "absolute", left: 0, right: 0, top: 0, bottom: 0 };
const urlParams = new URLSearchParams(window.location.search);

class MapApp extends Component {
  state = {
    zoom: urlParams.get("zoom") || 14,
    position: urlParams.get("position") ? urlParams.get("position").split(",") : position,
  };
  render() {
    return (
      <Vehicle>
        {vehicles => {
          return (
            <div style={cover}>
              <Map center={this.state.position} zoom={this.state.zoom} style={cover} zoomControl={false}>
                <TileLayer
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                />
              
                {vehicles.map(vehicle => {
                  const { routeNumber, type } = vehicle;
                  let classes = {
                    ["marker"]: true,
                  };
                  if (type === 1) {
                    classes[`tram`] = true;
                  } else if (type === 2) {
                    classes[`trolleybus`] = true;
                  }

                  const icon = divIcon({
                    className: classnames(classes),
                    html: `<span>${routeNumber}</span>`,
                  });

                  return (
                    <Marker
                      icon={icon}
                      key={vehicle.routeNumber+ (Math.random() *(20 -1)+1 )}
                      position={[vehicle.latitude, vehicle.longitude]}
                    />
                  );
                })}
              </Map>
            </div>
          );
        }}
      </Vehicle>
    );
  }
}

export default MapApp;