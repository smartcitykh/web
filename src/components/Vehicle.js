import { Component } from "react";
import io from "socket.io-client";

class Vehicle extends Component {
  state = {
    vehicles: [],
  };
  async componentDidMount() {
    this.socket = io("http://localhost:3001/", {
      transports: ["websocket"],
    });
    this.socket.on("vehicles_update", data =>
      this.setState({
        vehicles: data,
      }),
    );
  }

  render() {
    return this.props.children(this.state.vehicles);
  }
}

export default Vehicle;