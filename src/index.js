import React from "react";
import ReactDOM from "react-dom";
import MapApp from "./components/Map"

import "./styles.css";
import SideBar from "./sidebar";

function App() {
  return (
    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />

      <div id="page-wrap">
        
      </div>
    </div>
  );
}

const menuElement = document.getElementById("menu");
ReactDOM.render(<App />, menuElement);
ReactDOM.render(<MapApp />, document.getElementById("root"));
